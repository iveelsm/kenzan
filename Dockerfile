FROM adoptopenjdk/openjdk11

ENV HOME=/home/app/kenzan
RUN mkdir -p $HOME
WORKDIR $HOME

COPY package/* $HOME/
COPY build/libs/kenzan-* $HOME/kenzan.jar
RUN chmod 764 $HOME/kenzan.jar

RUN ln -s $HOME/kenzan.sh /usr/local/bin/kenzan

EXPOSE 8080
ENTRYPOINT ["kenzan"]