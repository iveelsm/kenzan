# Kenzan API

This project represents the fulfillment of the requirements for the Kenzan coding challenge.

## Requirements 

Create a web application that exposes REST operations for employees. The API should be able to:

* Get employees by an ID
* Create new employees
* Update existing employees
* Delete employees
* Get all employees

An employee is made up of the following data:

### Employee

* ID - Unique identifier for an employee
* FirstName - Employees first name
* MiddleInitial - Employees middle initial
* LastName - Employeed last name
* DateOfBirth - Employee birthday and year
* DateOfEmployment - Employee start date
* Status - ACTIVE or INACTIVE

### Startup

On startup, the application should be able to ingest an external source of employees, and should make them available via the GET endpoint.

### ACTIVE vs INACTIVE employees

By default, all employees are active, but by way of the API, can be switched to inactive. This should be done by the delete API call. This call should require some manner of authorization header.
When an employee is inactive, they should no longer be able to be retrieved in either the get by id, or get all employees calls

All data should be persisted into either memory, or externally. Please include instructions on how to run and interact with the web server.

## Project Specifics

### Startup Requirements

This project utilizing the following dependencies

* Gradle 5.4.1 (can be created with the gradle wrapper task if gradlew is not present)
* Java 11
* Docker
* Docker Compose
* Python 2.7
* Requests Python Module
* Make (not required, but will make life easier)

### How to Start

If `make` is installed... run the following command.

```shell script
make run
```

This will compile the jar, build the docker file and start the compose API.

If `make` is not installed... run the following commands...
```shell script
./gradlew clean build
docker-compose build
docker-compose up
```

### Test Employees!

If you would like to add some test employees to the API. The `scripts` directory is provided for convenience.

`generate_employees.py` will utilize the *baby.csv* and *surnames.csv* files to generate a number of employees.
The number of employees it generates is random, somewhere between 200 and 400. 
This value can be easily adjusted in the script. If you would like to try millions of employees, make sure you have allocated proper space for Docker.
The output will be in *employees.csv*

`load_employees.py` will make requests to the API and add the employees to the API. This will read from *employees.csv*.

### Make Some Requests!

A generic Postman collection that hits all endpoints has been provided. 
See the `postman` directory for details.

## Design Decisions

### Database

MongoDB was chosen simply because it was mentioned in the job description. Mongo has downsides and upsides as with any technology.
Some of the downsides are the consistency guarantees, although the latest Jepsen's indicate they have made significant strides here.
And the upside of "schema-less" is also a downside when misused.

Cassandra was overkill for this project even though it was also mentioned in the job description. Cassandra's LSM tree compaction strategies are phenomenal for write-heavy operations.
This is not a reasonable use for Cassandra, even though I do like Cassandra a lot.

### Caching

I implemented a caching layer because I have a pretty huge bias towards performant APIs. I think performance is one of the biggest indicators to a product's success.
And therefore I tend to bridge into into my day-to-day engineering practice.
The other benefit is we get a bit of a translation layer between the API level and the backend DB implementations.
This will enable more flexibility in the future

### Package Structure

One of the ways this comes out in my design is that I design around use-cases.
When the requirements are listed as "get employees", "create employees", "update", etc. I see a variety of business use-cases that need to be implemented.
The simplest way to communicate and extend this logic is to create abstractions based around domains and use-cases.
In this case, employee is our domain, and actions are our use case.
Which results in a package structure that represents those ideas.

However, this results in a fair bit of duplicated code as the keen observer will notice. 
Which brings me to my most controversial opinion that I hold. 
I believe that you should prefer duplication over abstraction intended to eliminate duplication.

