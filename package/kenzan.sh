#!/usr/bin/env bash

JAVA_PROPS_FILE=kenzan.properties

function java_properties() {
  while IFS="\n" read -r line; do
    if [[ "$line" != \#* ]]; then
      echo $line
    fi
  done < $JAVA_PROPS_FILE
}

java $(java_properties) -jar kenzan.jar