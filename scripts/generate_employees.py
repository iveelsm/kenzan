#!/usr/bin/env python

import random

baby_file="./baby.csv"
surname_file="./surnames.csv"
employee_file="./employees.csv"
letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def read_baby_names():
    ret = []
    with open(baby_file, "r") as f:
        header = f.readline()
        line = f.readline()
        while line:
            split = line.split(",")
            ret.append(split[1][1:-1])
            line = f.readline()
    return ret


def read_surnames():
    ret = []
    with open(surname_file, "r") as f:
        header = f.readline()
        line = f.readline()
        while line:
            split = line.split(",")
            ret.append(split[0].lower().capitalize())
            line = f.readline()
    return ret

def generate_birthday():
    year = random.randint(1920, 2000)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    return str(year) + "-" + str(month) + "-" + str(day)

def generate_employed_at():
    year = random.randint(1990, 2020)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    return str(year) + "-" + str(month) + "-" + str(day)

def generate_middle_initial():
    return random.choice(letters)

def generate_employee(baby_names, surnames):
    firstName = baby_names[random.randint(0, len(baby_names) - 1)]
    lastName = surnames[random.randint(0, len(surnames) - 1)]
    birthday = generate_birthday()
    employedAt = generate_employed_at()
    middleInitial = generate_middle_initial()
    return {
        "firstName": firstName,
        "middleInitial": middleInitial,
        "lastName": lastName,
        "birthday": birthday,
        "employedAt": employedAt
    }

def write_employee_csv(baby_names, surnames):
    with open(employee_file, "w") as f:
        f.write("firstName,middleInitial,lastName,birthday,employedAt\n")
        numEmployees = random.randint(200, 400)
        while numEmployees > 0:
            employee = generate_employee(baby_names, surnames)
            to_write = employee["firstName"] + "," \
                    + employee["middleInitial"] + "," \
                    + employee["lastName"] + "," \
                    + employee["birthday"] + "," \
                    + employee["employedAt"] + "\n"
            f.write(to_write)
            numEmployees = numEmployees - 1

baby_names = read_baby_names()
surnames = read_surnames()
write_employee_csv(baby_names, surnames)

