#!/usr/bin/env python

import requests
import json

employees_file="./employees.csv"

def send_employee(headers, line):
    headers = headers.split(",")
    split = line.split(",")
    data = {
        str(headers[0]): str(split[0]),
        str(headers[1]): str(split[1]),
        str(headers[2]): str(split[2]),
        str(headers[3]): str(split[3]),
        str(headers[4]): str(split[4])
    }
    print(json.dumps(data))
    headers = {'content-type': 'application/json'}
    r = requests.post("http://localhost:8080/employee", data=json.dumps(data), headers=headers)

def post_employees():
    with open(employees_file, "r") as f:
        header = f.readline().rstrip()
        line = f.readline()
        while line:
            send_employee(header, line.rstrip())
            line = f.readline()

post_employees()