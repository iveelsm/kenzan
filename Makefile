.PHONY: clean compile build run down generate load

generate:
	(cd scripts && ./generate_employees.py)

down:
	docker-compose down

clean: down
	./gradlew -Dorg.gradle.warning.mode=all clean

compile: clean
	./gradlew -Dorg.gradle.warning.mode=all build

build: compile	
	docker-compose build

run: build
	docker-compose up

load:
	(cd scripts && ./load_employees.py)
