package io.iveelsm.kenzan.cache.mongo.codec;

import io.iveelsm.kenzan.Employee;
import org.bson.Document;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class DocumentCodecTest {
    @Test
    public void shouldDecodeDocument() throws Exception {
        Document d = new Document();
        d.put("id", UUID.randomUUID());
        d.put("firstName", "foo");
        d.put("middleInitial", "f");
        d.put("lastName", "bar");
        d.put("birthday", "2020-02-11");
        d.put("employedAt", "2020-02-11");
        d.put("createdAt", DateTime.now());
        d.put("updatedAt", DateTime.now());
        Employee e = DocumentCodec.decode(d, Employee.class);
        assertThat(e.getFirstName()).isEqualTo("foo");
    }

    @Test
    public void shouldEncodeDocument() throws Exception {
        DateTime now = DateTime.now();
        Employee e = new Employee("foo", 'f', "bar", now, now);
        Document d = DocumentCodec.encode(e);
        assertThat(d.get("firstName")).isEqualTo("foo");
    }
}
