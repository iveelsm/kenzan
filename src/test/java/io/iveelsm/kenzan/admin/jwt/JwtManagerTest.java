package io.iveelsm.kenzan.admin.jwt;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JwtManagerTest {
    @Test
    public void shouldIssueValidJwt() {
        String token = JwtManager.generate();
        boolean result = JwtManager.verify(token);
        assertThat(result).isEqualTo(true);
    }
}
