package io.iveelsm.kenzan.admin;

import lombok.Data;

@Data
public class PostAdminResult {
    private final String jwt;

    public static PostAdminResult fromToken(String token) {
        return new PostAdminResult(token);
    }
}
