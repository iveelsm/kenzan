package io.iveelsm.kenzan.admin;

import io.iveelsm.kenzan.admin.jwt.JwtManager;
import io.iveelsm.kenzan.cache.mongo.AdminsCollectionClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AdminController {
    private final AdminsCollectionClient client;

    /**
     * Handles authentication and logging in for the status management routes
     *
     * @param body Username and password combination for the admin user
     * @return This API has several return types
     *  401 -> The request is not authorized as the username/password combo is invalid
     *  200 -> The request was successful, a JWT token will be returned for interaction with other routes
     *  500 -> There was an unexpected error in the server
     */
    @RequestMapping(path = "/admin", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody PostAdminBody body) {
        try {
            log.info("Attempting to find admin for username {}", body.getUsername());
            var admin = this.client.getAdmin(body.getUsername(), body.getPassword());
            if(admin == null) {
                return new ResponseEntity<>("Not so fast.", HttpStatus.UNAUTHORIZED);
            } else {
                return new ResponseEntity<>(PostAdminResult.fromToken(JwtManager.generate()), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("There was an error on the request", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
