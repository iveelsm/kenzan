package io.iveelsm.kenzan.admin.jwt;


/**
 * Manages the current authentication scheme for the authentication path which utilizes Jwts.
 *
 * Provided as a means of encapsulating the implementation details of the JWT library.
 */
public class JwtManager {
    /**
     * Issues a JWT. Should only be used in cases where a reasonable level of authentication has already occurred to prevent nefarious use.
     *
     * @return A JWT that can be used for authentication protected routes.
     */
    public static String generate() {
        return JwtAlgorithm.issue();
    }

    /**
     * Verifies a potential token as issued by this API or not.
     *
     * @param token JWT-like token to test
     * @return True if valid, false otherwise
     */
    public static boolean verify(String token) {
        return JwtAlgorithm.verify(token);
    }
}
