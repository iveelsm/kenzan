package io.iveelsm.kenzan.admin.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Generic implementation of a JWT algorithm using the Auth0 library here: https://github.com/auth0/java-jwt
 */
@Slf4j
@Getter
class JwtAlgorithm {
    private static final String HMAC_SECRET = "sHh_1t5_@_s3cr3t";
    private static final Algorithm algorithm = Algorithm.HMAC256(HMAC_SECRET);

    static String issue() {
        try {
            Algorithm algorithm = Algorithm.HMAC256("secret");
            return JWT.create()
                    .withIssuer("kenzan")
                    .sign(algorithm);
        } catch (JWTCreationException e) {
            log.error("There was an issue generating the JWT", e);
            return null;
        }
    }

    static boolean verify(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256("secret");
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("kenzan")
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            // Basically if we don't throw, we have a valid token. Normally you would use the decoded data in some way.
            // However, for our case, we don't require it.
            return true;
        } catch (JWTVerificationException e){
            log.error("There was an issue verifiying the JWT", e);
            return false;
        }
    }

}
