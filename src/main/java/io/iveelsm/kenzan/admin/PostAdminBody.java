package io.iveelsm.kenzan.admin;

import lombok.Data;

@Data
public class PostAdminBody {
    private String username;
    private String password;
}
