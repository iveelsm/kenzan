package io.iveelsm.kenzan.cache;

import java.util.List;

/**
 * A cache is responsible for representing a small subset of heavily accessed data in memory.
 * Caches must be backed by a more persistent form of storage due to the capped nature.
 *
 * @param <K> Type signature for the keys
 * @param <V> Type signature for the values
 */
public interface Cache<K,V> {
    /**
     * Get the value from the cache, if it does not exist, load it.
     *
     * @param key Key to find the value by
     * @return Value for the key if it exists in the cache or data source backing the cache, null otherwise
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache
     */
    V get(K key) throws Exception;

    /**
     * Direct retrieval from the data source backing the cache.
     *
     * @param value Approximate value to retrieve
     * @return Value if it exists in the data source, null otherwise
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache
     */
    V retrieve(V value) throws Exception;

    /**
     * Batch retrieval using cursor based pagination
     *
     * @param num maximum number of entities to return
     * @param cursor current location in the list
     * @return List with a variety of values, less than or equal to the maximum number specified
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache
     */
    List<V> batchGet(int num, String cursor) throws Exception;

    /**
     * Put a value into the cache and underlying source.
     *
     * @param key Key to put for subsequent retrievals
     * @param value Value to put into the cache
     * @return Value that was placed in the cache
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache or putting data into the cache
     */
    V put(K key, V value) throws Exception;

    /**
     * Replaces an existing value in the cache and underlying source.
     *
     * @param key Key to put for subsequent retrievals
     * @param value Value to put into the cache
     * @return Value that was placed in the cache
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache or putting data into the cache
     */
    V replace(K key, V value) throws Exception;

    /**
     * Removes a value from the cache and underlying source.
     *
     * @param key Key to determine the value to remove
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache or putting data into the cache
     */
    void remove(K key) throws Exception;
}
