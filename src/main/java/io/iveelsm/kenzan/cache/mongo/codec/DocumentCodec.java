package io.iveelsm.kenzan.cache.mongo.codec;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;

/**
 * This class encodes data from a generic object type in a {@link Document document} that can be stored in Mongo.
 * Or similarly decodes data from a {@link Document document} into a generic object type.
 */
@Slf4j
public class DocumentCodec {
    private static final ObjectMapper mapper = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    /**
     * Converts an object into a {@link Document document} for interaction with MongoDB
     *
     * @param t Generic object to convert
     * @param <T> Type signature of object
     * @return Document representing a BSON version of the input
     * @throws ParsingException thrown on any issues converting the data. There are a variety of occurrences for this, including but not limited to:
     *  An issue processing the JSON data, which occurs with Jackson.
     *  An issue reading the data which caused an IOException. This is unlikely
     */
    public static <T> Document encode(T t) throws ParsingException {
        try {
            return Document.parse(mapper.writeValueAsString(t));
        } catch (Exception e) {
            log.info("There was an error serializing the data", e);
            throw new ParsingException(e.getMessage());
        }
    }

    /**
     * Converts a document into a generic object.
     *
     * @param document Document to convert into the object
     * @param clazz Class type to attempt to serialize into
     * @param <T> Type signature of the class
     * @return Generic object serialized from BSON representation, or null.
     * @throws ParsingException thrown when there is an issue construct the object. Can happen for a variety of reasons, including:
     *  If underlying input contains invalid content
     *  If the input JSON structure does not match structure
     */
    public static <T> T decode(Document document, Class<T> clazz) throws ParsingException {
        try {
            return mapper.readValue(document.toJson(), clazz);
        } catch(NullPointerException e) {
            log.warn("Document was null");
            return null;
        } catch(IllegalArgumentException e) {
            log.warn("Attempting to serialize null data");
            return null;
        } catch (Exception e) {
            log.info("There was an error serializing the data", e);
            throw new ParsingException(e.getMessage());
        }
    }
}
