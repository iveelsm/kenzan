package io.iveelsm.kenzan.cache.mongo.errors;

import java.io.IOException;

/**
 * Class of exceptions thrown when inserting data into MongoDB
 */
public class MongoInsertException extends IOException {
    public MongoInsertException(String message) {
        super(message);
    }
}
