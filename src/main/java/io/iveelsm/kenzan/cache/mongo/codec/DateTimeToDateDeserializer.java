package io.iveelsm.kenzan.cache.mongo.codec;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;

/**
 * This class deserializes DateTimes into an inexact format (YYYY-MM-DD).
 *
 * It is used by Jackson's {@link com.fasterxml.jackson.databind.ObjectMapper mapper} for understand how to deserialize a DateTime object.
 */
public class DateTimeToDateDeserializer extends StdScalarDeserializer<DateTime> {
    private static final DateTimeFormatter mongoDateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");

    public DateTimeToDateDeserializer() {
        super(DateTime.class);
    }

    @Override
    public DateTime deserialize(JsonParser jsonParser,
                                DeserializationContext deserializationContext) throws IOException {
        try {
            JsonToken currentToken = jsonParser.getCurrentToken();
            if (currentToken == JsonToken.VALUE_STRING) {
                String dateTimeAsString = jsonParser.getText().trim();
                return mongoDateTimeFormatter.parseDateTime(dateTimeAsString);
            }
            throw new ParsingException("Failed to find value string for DateTime");
        } catch(Exception e) {
            throw new ParsingException("Failure to parse DateTime");
        }
    }
}
