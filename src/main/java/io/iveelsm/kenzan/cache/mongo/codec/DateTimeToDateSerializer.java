package io.iveelsm.kenzan.cache.mongo.codec;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;

/**
 * This class serializes DateTimes into an inexact format (YYYY-MM-DD).
 *
 * It is used by Jackson's {@link com.fasterxml.jackson.databind.ObjectMapper mapper} for understand how to serialize a DateTime object.
 */
public class DateTimeToDateSerializer extends StdScalarSerializer<DateTime> {
    private static final DateTimeFormatter mongoDateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");

    public DateTimeToDateSerializer() {
        super(DateTime.class);
    }

    @Override
    public void serialize(DateTime dateTime,
                          JsonGenerator jsonGenerator,
                          SerializerProvider provider) throws IOException {
        String dateTimeAsString = dateTime.toString(mongoDateTimeFormatter);
        jsonGenerator.writeString(dateTimeAsString);
    }
}