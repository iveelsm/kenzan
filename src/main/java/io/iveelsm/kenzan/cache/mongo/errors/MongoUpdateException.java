package io.iveelsm.kenzan.cache.mongo.errors;

import java.io.IOException;

/**
 * Class of exception thrown when updating data in MongoDB.
 *
 * Could be constructed as a means of remove/insert, but for now is independent.
 */
public class MongoUpdateException extends IOException {
    public MongoUpdateException(String message) {
        super(message);
    }
}
