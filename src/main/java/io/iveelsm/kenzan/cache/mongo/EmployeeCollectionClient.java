package io.iveelsm.kenzan.cache.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.iveelsm.kenzan.Employee;
import io.iveelsm.kenzan.cache.mongo.codec.DocumentCodec;
import io.iveelsm.kenzan.cache.mongo.errors.MongoGetException;
import io.iveelsm.kenzan.cache.mongo.errors.MongoInsertException;
import io.iveelsm.kenzan.cache.mongo.errors.MongoRemoveException;
import io.iveelsm.kenzan.cache.mongo.errors.MongoUpdateException;
import org.bson.Document;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * This class handles all the interactions with the MongoDB collection called "employees"
 */
@Component
public class EmployeeCollectionClient {
    private final LocalMongoClient client;
    private final MongoDatabase database;
    private final MongoCollection<Document> collection;

    public EmployeeCollectionClient() {
        this.client = new LocalMongoClient();
        this.database = this.client.getClient().getDatabase("kenzan");
        this.collection = this.database.getCollection("employees");
    }

    /**
     * Gets an active employee as referenced by an ID
     *
     * @param id Identifier for the potential employee
     * @return Employee object if the ID is active and exists, null otherwise
     * @throws MongoGetException thrown on issue with serialization or connection to the MongoDB collection
     */
    public Employee getActiveEmployee(UUID id) throws MongoGetException {
        try {
            List<DBObject> criteria = new ArrayList<>();
            criteria.add(new BasicDBObject("id", id.toString()));
            criteria.add(new BasicDBObject("status", "ACTIVE"));
            return DocumentCodec.decode(this.collection.find(new BasicDBObject("$and", criteria))
                    .limit(1)
                    .first(), Employee.class);
        } catch (Exception e) {
            throw new MongoGetException(e.getMessage());
        }
    }

    /**
     * Gets an employee by checking a variety of fields that can be considered unique.
     * It is assumed that the combination of firstName, middleInitial, lastName, birthday and employedAt are sufficient uniqueness constraints.
     *
     * @param employee Employee to attempt to find in the MongoDB collection
     * @return Employee that matches the uniqueness constraint defined above, null otherwise
     * @throws MongoGetException thrown on issue with serialization or connection to the MongoDB collection
     */
    public Employee getEmployee(Employee employee) throws MongoGetException {
        try {
            Document document = DocumentCodec.encode(employee);
            List<DBObject> criteria = new ArrayList<>();
            criteria.add(new BasicDBObject("firstName", document.get("firstName")));
            criteria.add(new BasicDBObject("middleInitial", document.get("middleInitial")));
            criteria.add(new BasicDBObject("lastName", document.get("lastName")));
            criteria.add(new BasicDBObject("birthday", document.get("birthday")));
            criteria.add(new BasicDBObject("employedAt", document.get("employedAt")));
            return DocumentCodec.decode(this.collection.find(new BasicDBObject("$and", criteria))
                    .limit(1)
                    .first(), Employee.class);
        } catch (Exception e) {
            throw new MongoGetException(e.getMessage());
        }
    }

    /**
     * I am going to skip the normal JavaDoc comment. As this is one of the few functions I want to actually speak on.
     * This function is my implementation of "get all" employees.
     * When we think about getting all employees, there is definitely a limit to what "all" means.
     * There are a few ways to handle this limit, however one of the best ways I have found to handle this is cursor-based pagination.
     *
     * For those interested you can read about it here: https://graphql.org/learn/pagination/ albeit in a very different context.
     *
     * @param pageSize Maximum number of results to return in a page
     * @param cursor Current cursor
     * @return List of employees that is less than or equal to the page size
     * @throws MongoGetException thrown on an issue with serialization or connection to the database
     */
    public List<Employee> batchGetEmployees(int pageSize, String cursor) throws MongoGetException {
        try {
            FindIterable<Document> documents;
            if (cursor != null) {
                List<DBObject> criteria = new ArrayList<>();
                criteria.add(new BasicDBObject("id", new BasicDBObject("$lt", cursor)));
                criteria.add(new BasicDBObject("status", "ACTIVE"));
                documents = this.collection.find(new BasicDBObject("$and", criteria))
                        .sort(new BasicDBObject("id", -1))
                        .limit(pageSize);
            } else {
                documents = this.collection.find(new BasicDBObject("status", "ACTIVE"))
                        .sort(new BasicDBObject("id", -1)).limit(pageSize);
            }
            List<Employee> ret = new ArrayList<>();
            for(var document : documents) {
                ret.add(DocumentCodec.decode(document, Employee.class));
            }
            return ret;
        } catch (Exception e) {
            throw new MongoGetException(e.getMessage());
        }
    }

    /**
     * Inserts an employee into MongoDB
     *
     * @param employee Employee to insert into MongoDB
     * @throws MongoInsertException thrown on an issue with the MongoDB connection
     */
    public void putEmployee(Employee employee) throws MongoInsertException {
        try {
            this.collection.insertOne(DocumentCodec.encode(employee));
        } catch (Exception e) {
            throw new MongoInsertException(e.getMessage());
        }
    }

    /**
     * Updates an employee by replacing the document with the new data.
     *
     * @param employee Employee to update in the database
     * @throws MongoUpdateException thrown on an issue with the MongoDB connection, or if a write was not acknowledged.
     */
    public void updateEmployee(Employee employee) throws MongoUpdateException {
        try {
            var result = this.collection.replaceOne(new Document("id", employee.getId()), DocumentCodec.encode(employee));
            if (!result.wasAcknowledged()) {
                throw new Exception("There was an issue with the update to Mongo, the write was not acknowledged");
            }
        } catch (Exception e) {
            throw new MongoUpdateException(e.getMessage());
        }
    }

    /**
     * Completely removes the employee from the MongoDB collection
     *
     * @param employee Employee to remove from the MongoDB collection
     * @return True if the removal was acknowledged, false otherwise
     * @throws MongoRemoveException thrown with an issue with the MongoDB connection
     */
    public boolean removeEmployee(Employee employee) throws MongoRemoveException {
        try {
            var result = this.collection.deleteOne(new Document("id", employee.getId().toString()));
            return result.wasAcknowledged();
        } catch (Exception e) {
            throw new MongoRemoveException(e.getMessage());
        }
    }

    /**
     * Modifies an employee to be inactive. Used by removal routes.
     *
     * @param employee Employee to mark inactive.
     * @throws MongoRemoveException thrown with an issue with the MongoDB connection
     */
    public void makeEmployeeInactive(Employee employee) throws MongoRemoveException {
        try {
            employee.setStatus(Employee.Status.INACTIVE);
            this.updateEmployee(employee);
        } catch (Exception e) {
            throw new MongoRemoveException(e.getMessage());
        }
    }
}
