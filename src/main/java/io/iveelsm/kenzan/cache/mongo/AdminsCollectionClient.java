package io.iveelsm.kenzan.cache.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.iveelsm.kenzan.Admin;
import io.iveelsm.kenzan.cache.mongo.codec.DocumentCodec;
import io.iveelsm.kenzan.cache.mongo.errors.MongoGetException;
import org.bson.Document;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * This class handles all the interactions with the MongoDB collection called "admin"
 */
@Component
public class AdminsCollectionClient {
    private final LocalMongoClient client;
    private final MongoDatabase database;
    private final MongoCollection<Document> collection;

    private static final String ADMIN_USERNAME = "admin";
    // secret for the reviewer...
    // I really wanted to build a crypto challenge into the API as an easter egg but I decided to take a more professional route.
    // But I left the choice for the password.
    private static final String ADMIN_PASSWORD = "can-u-gu3ss-m3";
    private static final String SALT = BCrypt.gensalt();

    public AdminsCollectionClient() {
        this.client = new LocalMongoClient();
        this.database = this.client.getClient().getDatabase("kenzan");
        this.collection = this.database.getCollection("admins");
        addAdmin();
    }

    /**
     * Currently the only means by which an admin can be added.
     * Since there is only one admin, this would need to be externalized if more were required.
     */
    private void addAdmin() {
        String password = BCrypt.hashpw(ADMIN_PASSWORD, SALT);
        var admin = new Admin(ADMIN_USERNAME, password);
        try {
            this.collection.insertOne(DocumentCodec.encode(admin));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Attempts to find an admin by username/password combination for authentication with API.
     *
     * @param username Username for the admin
     * @param password Password for the admin
     * @return An Admin object upon successful username/password combination, null otherwise.
     * @throws MongoGetException thrown in cases of unexpected exceptions with the MongoDB client or with the serialization process
     */
    public Admin getAdmin(String username, String password) throws MongoGetException {
        String hashed = BCrypt.hashpw(password, SALT);
        var admin = new Admin(username, hashed);
        try {
            Document document = DocumentCodec.encode(admin);
            List<DBObject> criteria = new ArrayList<>();
            criteria.add(new BasicDBObject("username", document.get("username")));
            criteria.add(new BasicDBObject("password", document.get("password")));
            var result = this.collection.find(new BasicDBObject("$and", criteria))
                    .limit(1)
                    .first();
            return (result == null) ? null : DocumentCodec.decode(result, Admin.class);
        } catch (Exception e) {
            throw new MongoGetException(e.getMessage());
        }
    }

}
