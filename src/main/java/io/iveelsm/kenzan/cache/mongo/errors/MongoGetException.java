package io.iveelsm.kenzan.cache.mongo.errors;


import java.io.IOException;

/**
 * Class of exceptions thrown when retrieving data from MongoDB
 */
public class MongoGetException extends IOException {
    public MongoGetException(String message) {
        super(message);
    }
}
