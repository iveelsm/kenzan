package io.iveelsm.kenzan.cache.mongo.errors;


import java.io.IOException;

/**
 * Class of exceptions thrown when removing data from MongoDB
 */
public class MongoRemoveException extends IOException {
    public MongoRemoveException(String message) {
        super(message);
    }
}
