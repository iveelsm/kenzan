package io.iveelsm.kenzan.cache.mongo;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.Getter;
import org.springframework.stereotype.Component;

/**
 * Connects to the local MongoDB. Hostname is "mongo" due to the way Docker Compose networking is setup.
 */
@Getter
@Component
class LocalMongoClient {
    private final MongoClient client;

    LocalMongoClient() {
        this.client = MongoClients.create("mongodb://mongo:27017");
    }
}
