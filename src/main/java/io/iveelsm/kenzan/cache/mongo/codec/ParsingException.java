package io.iveelsm.kenzan.cache.mongo.codec;

import java.io.IOException;

/**
 * Thrown in cases where we are unable to parse JSON data, or convert it into another representation.
 */
public class ParsingException extends IOException {
    public ParsingException(String message) {
        super(message);
    }
}
