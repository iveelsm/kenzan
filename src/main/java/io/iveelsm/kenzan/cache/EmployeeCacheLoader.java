package io.iveelsm.kenzan.cache;

import com.google.common.cache.CacheLoader;
import io.iveelsm.kenzan.Employee;
import io.iveelsm.kenzan.cache.mongo.EmployeeCollectionClient;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.UUID;

/**
 * Loads employees into a fixed size cache.
 */
public class EmployeeCacheLoader extends CacheLoader<UUID, Employee> {
    private final EmployeeCollectionClient client;

    public EmployeeCacheLoader(EmployeeCollectionClient client) {
        this.client = client;
    }

    @Override
    @ParametersAreNonnullByDefault
    public Employee load(UUID key) throws Exception {
        return this.client.getActiveEmployee(key);
    }
}
