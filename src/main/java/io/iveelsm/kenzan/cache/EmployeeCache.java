package io.iveelsm.kenzan.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import io.iveelsm.kenzan.Employee;
import io.iveelsm.kenzan.cache.mongo.EmployeeCollectionClient;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * An employee cache holds a certain number of employees in member for quick access from read operations.
 */
@Component
public class EmployeeCache implements Cache<UUID, Employee> {
    private static final int MAX_SIZE = 500;

    private final LoadingCache<UUID, Employee> cache;
    private final EmployeeCollectionClient client;

    public EmployeeCache() {
        this.client = new EmployeeCollectionClient();
        this.cache = CacheBuilder.newBuilder()
                .maximumSize(MAX_SIZE)
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .build(new EmployeeCacheLoader(client));
    }

    /**
     * Get the value from the cache, if it does not exist, load it.
     *
     * @param key Key to find the value by
     * @return Employee for the key if it exists in the cache or data source backing the cache, null otherwise
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache
     */
    @Override
    public Employee get(UUID key) throws Exception {
        return cache.get(key);
    }

    /**
     * Direct retrieval from the data source backing the cache.
     *
     * @param e Employee to attempt to retrieve
     * @return Employee if it exists in the data source, null otherwise
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache
     */
    @Override
    public Employee retrieve(Employee e) throws Exception {
        return client.getEmployee(e);
    }

    /**
     * Batch retrieval using cursor based pagination
     *
     * @param num maximum number of entities to return
     * @param cursor current location in the list
     * @return List with a set of employees, less than or equal to the maximum number specified
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache
     */
    @Override
    public List<Employee> batchGet(int num, String cursor) throws Exception {
        return client.batchGetEmployees(num, cursor);
    }

    /**
     * Put an employee into the cache and underlying source.
     *
     * @param key Key to put for subsequent retrievals
     * @param value Employee to put into the cache
     * @return Employee that was placed in the cache
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache or putting data into the cache
     */
    @Override
    public Employee put(UUID key, Employee value) throws Exception {
        client.putEmployee(value);
        return cache.get(key);
    }

    /**
     * Replaces an employee in the cache and underlying source.
     *
     * @param key Key to put for subsequent retrievals
     * @param value Employee to replace into the cache
     * @return Employee that was replaced in the cache
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache or putting data into the cache
     */
    @Override
    public Employee replace(UUID key, Employee value) throws Exception {
        client.updateEmployee(value);
        return cache.get(key);
    }

    /**
     * Removes an employee from the cache and underlying source.
     *
     * @param key Key to determine the value to remove
     * @throws Exception thrown if there is an unexpected issue retrieving the data from the cache or putting data into the cache
     */
    @Override
    public void remove(UUID key) throws Exception {
        client.makeEmployeeInactive(cache.get(key));
        cache.invalidate(key);
    }
}
