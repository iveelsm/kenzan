package io.iveelsm.kenzan.modify;

import io.iveelsm.kenzan.Employee;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * This represents the input to the PATCH /employee/{id} route.
 *
 * Modifications to this class may require backwards compatibility.
 */
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
class ModifyEmployeeBody {
    private String firstName;
    private Character middleInitial;
    private String lastName;
    private String birthday;
    private String employedAt;

    /**
     * Constructs an employee from an exist employee and potential modifications to apply if they exist.
     *
     * @param e Existing employee representation
     * @param modify Modifications to apply to the employee
     * @return Consolidated view of the Employee, giving preference to any defined properties in the modifications
     */
    static Employee toEmployee(Employee e, ModifyEmployeeBody modify) {
        return new Employee(
                e.getId(),
                (modify.getFirstName() != null) ? modify.getFirstName() : e.getFirstName(),
                (modify.getMiddleInitial() != null) ? modify.getMiddleInitial() : e.getMiddleInitial(),
                (modify.getLastName() != null) ? modify.getLastName() : e.getLastName(),
                (modify.getBirthday() != null) ? Employee.getBirthdayFormatter().parseDateTime(modify.getBirthday()) : e.getBirthday(),
                (modify.getEmployedAt() != null) ? Employee.getBirthdayFormatter().parseDateTime(modify.getEmployedAt()) : e.getEmployedAt(),
                e.getStatus(),
                e.getCreatedAt(),
                DateTime.now(DateTimeZone.UTC)
        );
    }
}
