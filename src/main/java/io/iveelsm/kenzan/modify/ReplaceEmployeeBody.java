package io.iveelsm.kenzan.modify;

import io.iveelsm.kenzan.Employee;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.UUID;

/**
 * This represents the input to the PUT /employee/{id} route.
 *
 * Modifications to this class may require backwards compatibility.
 */
@Getter
@RequiredArgsConstructor
class ReplaceEmployeeBody {
    private final String firstName;
    private final Character middleInitial;
    private final String lastName;
    private final String birthday;
    private final String employedAt;

    /**
     * Constructs an employee from an existing employee and replacement employee body
     * All fields can be nullable except for ID, Status, Created At and Updated At.
     *
     * @param id Unique identifier for the employee
     * @param body Replacement data for the employee, if it exists
     * @param employee Existing employee, can be null
     * @return Employee from the existing, if present, and replacement data
     */
    static Employee toEmployee(UUID id, ReplaceEmployeeBody body, Employee employee) {
        return new Employee(
                id,
                body.firstName,
                body.middleInitial,
                body.lastName,
                Employee.getBirthdayFormatter().parseDateTime(body.birthday),
                Employee.getEmployedAtFormatter().parseDateTime(body.employedAt),
                (employee != null) ? employee.getStatus() : Employee.Status.ACTIVE,
                (employee != null) ? employee.getCreatedAt() : DateTime.now(DateTimeZone.UTC),
                DateTime.now(DateTimeZone.UTC)
        );
    }
}