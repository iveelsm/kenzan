package io.iveelsm.kenzan.modify;

import io.iveelsm.kenzan.Employee;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.UUID;

/**
 * This is the JSON object representation of the return type of the PUT /employee/{id} route in the API.
 *
 * This refers to only the UUID as an interpretation of RFC 7231 (https://tools.ietf.org/html/rfc7231)
 * Given that POST routes are expected to return a means of identification of the new resources upon 201 CREATED responses.
 * It was interpreted that PUT should result in the same result.
 *
 * Modifications may require backwards compatibility.
 */
@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
class ReplaceEmployeeResult {
    private final UUID id;

    /**
     * Constructs the JSON return POJO from an existing Employee.
     *
     * @param e Employee to use for construction of the POJO
     * @return A JSON object representing the response to PUT /employee/{id} route
     */
    static ReplaceEmployeeResult fromEmployee(Employee e) {
        return new ReplaceEmployeeResult(e.getId());
    }
}