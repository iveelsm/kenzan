package io.iveelsm.kenzan.modify;

import io.iveelsm.kenzan.Employee;
import io.iveelsm.kenzan.cache.Cache;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ModifyController {
    private final Cache<UUID, Employee> cache;

    /**
     * Updates employee properties. All properties are available to be updated except for ID and {@link Employee.Status status}
     *
     * @param id The unique identifier (UUID) that indicates which employee to modify
     * @param body The modifications to perform on the employee. All fields are optional.
     * @return This API has several different possible returns:
     *  422 -> No employee exists for the given ID
     *  200 -> The employee has been updated, a view of the employee will be return to the consumer
     *  400 -> The id was not in a UUID format
     *  500 -> An unexpected error occurred on the request
     */
    @RequestMapping(path = "/employee/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> updateEmployee(@PathVariable String id, @RequestBody ModifyEmployeeBody body) {
        try {
            log.info("Updating employee {}", id);
            UUID uuid = UUID.fromString(id);
            Employee current = cache.get(uuid);
            if(current == null) {
                log.warn("No employee exists for id {}", id);
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            var merged = ModifyEmployeeBody.toEmployee(current, body);
            cache.put(uuid, merged);
            log.info("Employee {} updated successfully", id);
            return new ResponseEntity<>(ModifyEmployeeResult.fromEmployee(merged), HttpStatus.OK);
        } catch(IllegalArgumentException e) {
            log.error("There was an error trying to serialize {} into a UUID", id);
            return new ResponseEntity<>("ID is not in a UUID format", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.error("There was an error on the request", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Replaces an employee in it's entirety.
     *
     * @param id The unique identifier (UUID) that indicates which employee to replace
     * @param body The employee data to replace the existing, if it exists. All fields are optional, but recommended.
     * @return This API has several different possible returns:
     *  201 -> No employee exists for the given ID, one was constructed based on what was provided. A view of the employee will be returned to the caller.
     *  204 -> The employee has been replaced.
     *  400 -> The id was not in a UUID format
     *  500 -> An unexpected error occurred on the request
     */
    @RequestMapping(path = "/employee/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> replaceEmployee(@PathVariable String id, @RequestBody ReplaceEmployeeBody body) {
        try {
            log.info("Replacing employee...");
            UUID uuid = UUID.fromString(id);
            var current = cache.get(uuid);
            Employee employee = (current == null)
                    ? ReplaceEmployeeBody.toEmployee(uuid, body, null)
                    : ReplaceEmployeeBody.toEmployee(uuid, body, current);
            cache.replace(employee.getId(), employee);
            log.info("Employee {} successfully replaced", employee.getId());
            return (current != null)
                ? new ResponseEntity<>(null, HttpStatus.NO_CONTENT)
                : new ResponseEntity<>(ReplaceEmployeeResult.fromEmployee(employee), HttpStatus.CREATED);
        } catch(IllegalArgumentException e) {
            log.error("There was an error trying to serialize {} into a UUID", id);
            return new ResponseEntity<>("ID is not in a UUID format", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.error("There was an error on the request", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
