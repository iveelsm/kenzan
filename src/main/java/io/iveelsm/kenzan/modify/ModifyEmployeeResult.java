package io.iveelsm.kenzan.modify;

import io.iveelsm.kenzan.Employee;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.UUID;

/**
 * This is the JSON object representation of the return type of the PATCH /employee/{id} route in the API.
 *
 * Modifications may require backwards compatibility.
 */
@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
class ModifyEmployeeResult {
    private final UUID id;
    private final String firstName;
    private final Character middleInitial;
    private final String lastName;
    private final String birthday;
    private final String employedAt;
    private final Employee.Status status;

    /**
     * Constructs a result view from the information in an {@link Employee employee} entity.
     *
     * @param e Employee to use in the return
     * @return A JSON object representing the return from the PATCH /employees route.
     */
    static ModifyEmployeeResult fromEmployee(Employee e) {
        return new ModifyEmployeeResult(
                e.getId(),
                e.getFirstName(),
                e.getMiddleInitial(),
                e.getLastName(),
                e.getBirthday().toString(Employee.getBirthdayFormatter()),
                e.getEmployedAt().toString(Employee.getEmployedAtFormatter()),
                e.getStatus()
        );
    }
}