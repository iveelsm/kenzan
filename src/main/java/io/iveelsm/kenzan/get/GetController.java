package io.iveelsm.kenzan.get;

import io.iveelsm.kenzan.Employee;
import io.iveelsm.kenzan.cache.Cache;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class GetController {
    private final Cache<UUID, Employee> cache;

    /**
     * Gets an employee by an ID reference
     *
     * @param id Identifier for an employee in UUID format
     * @return This API has several return types:
     *  200 -> Successful get with employee returned
     *  404 -> No entity found for the referenced ID
     *  400 -> The ID was not in UUID format
     *  500 -> There was an unexpected error in the server
     */
    @RequestMapping(path = "/employee/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getEmployee(@PathVariable String id) {
        try {
            log.info("Retrieving employee from cache for ID {}", id);
            var employee = cache.get(UUID.fromString(id));
            log.debug("Employee found in cache: {}", employee);
            return (employee != null)
                    ? new ResponseEntity<>(GetEmployeeResult.fromEmployee(employee), HttpStatus.OK)
                    : new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch(IllegalArgumentException e) {
            log.error("There was an error trying to serialize {} into a UUID", id);
            return new ResponseEntity<>("ID is not in a UUID format", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.error("There was an error on the request", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Paginated batch get for all employees
     *
     * @param pageSize Maximum number of employees to return, defaults to 20
     * @param cursor Current location in the list, null if starting a new pagination request
     * @return This API has several return types:
     *  200 -> Successful call with a list of employees bounded by pageSize parameter
     *  500 -> There was an unexpected error in the server
     */
    @RequestMapping(path = "/employees", method = RequestMethod.GET)
    public ResponseEntity<?> getEmployees(@RequestParam(defaultValue = "20") Integer pageSize,
                                          @RequestParam(required = false) String cursor) {
        try {
            List<Employee> employees = cache.batchGet(pageSize, cursor);
            if (employees.size() < pageSize) {
                return new ResponseEntity<>(GetEmployeesResult.fromEmployees(employees, null), HttpStatus.OK);
            } else {
                var last = employees.get(employees.size() - 1);
                return new ResponseEntity<>(GetEmployeesResult.fromEmployees(employees, last.getId().toString()), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("There was an error on the request", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
