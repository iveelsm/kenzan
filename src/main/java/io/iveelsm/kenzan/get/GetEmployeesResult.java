package io.iveelsm.kenzan.get;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.iveelsm.kenzan.Employee;
import lombok.Data;

import java.util.List;

/**
 * This is the JSON object representation of the return type of the GET /employee/{id} route in the API.
 *
 * Modifications may require backwards compatibility.
 */
@Data
class GetEmployeesResult {
    private final List<Employee> employees;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String cursor;

    /**
     * Constructs a result view from the information in an {@link Employee employee} entity.
     *
     * @param employees Employees to use in the return
     * @param cursor Current location in the paginated list
     * @return A JSON object representing the return from the GET /employees route.
     */
    public static GetEmployeesResult fromEmployees(List<Employee> employees, String cursor) {
        return new GetEmployeesResult(employees, cursor);
    }
}
