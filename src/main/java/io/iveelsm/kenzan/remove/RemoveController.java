package io.iveelsm.kenzan.remove;

import io.iveelsm.kenzan.Employee;
import io.iveelsm.kenzan.cache.Cache;
import io.iveelsm.kenzan.admin.jwt.JwtManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class RemoveController {
    private final Cache<UUID, Employee> cache;

    /**
     * This route is responsible for removing an employee.
     * In this case, removal indicates marking an employee as Inactive as indicated by their {@link Employee.Status status}
     *
     * @param token JWT token responsible for authorizing the call. Only admins who are actively logged in are able to adjust employee {@link Employee.Status status}
     * @param id The unique identifier (UUID) that indicates which employee to remove.
     * @return There are a few possible returns for this API:
     *  401 -> The request was unauthorized, or the JWT was invalid
     *  204 -> The request was successful and the employee has been marked Inactive
     *  400 -> The id was not in a UUID format
     *  500 -> An unexpected error occurred in the implementation
     */
    @RequestMapping(path = "/employee/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeEmployee(@RequestHeader("Authorization") String token, @PathVariable String id) {
        if(!verifyHeader(token)) {
           return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }

        try {
            log.info("Removing employee {}", id);
            cache.remove(UUID.fromString(id));
            log.info("Employee remove call was successful");
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch(IllegalArgumentException e) {
            log.error("There was an error trying to serialize {} into a UUID", id);
            return new ResponseEntity<>("ID is not in a UUID format", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.error("There was an error on the request", e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean verifyHeader(String token) {
        if(token == null) {
            return false;
        }
        try {
            return JwtManager.verify(token.split(" ")[1]);
        } catch(Exception e) {
            log.error("There was an error verifyin the JWT", e);
            return false;
        }
    }
}
