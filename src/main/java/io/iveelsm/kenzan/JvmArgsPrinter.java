package io.iveelsm.kenzan;

import lombok.extern.slf4j.Slf4j;

import java.lang.management.ManagementFactory;
import java.util.List;

@Slf4j
public class JvmArgsPrinter {
    /**
     * Prints the JVM Arguments passed into the process.
     * This class is important for understanding how a given process was run.
     * As well as understanding if there are areas for improvement with regards to JVM flags
     */
    static void print() {
        log.info("JVM Arguments");
        getArgs().forEach(log::info);
    }

    static List<String> getArgs() {
        return ManagementFactory.getRuntimeMXBean().getInputArguments();
    }
}
