package io.iveelsm.kenzan.create;

import io.iveelsm.kenzan.Employee;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * This represents the input to the POST /employee route.
 *
 * Modifications to this class may require backwards compatibility.
 */
@Getter
@RequiredArgsConstructor
class CreateEmployeeBody {
    private final String firstName;
    private final Character middleInitial;
    private final String lastName;
    private final String birthday;
    private final String employedAt;

    /**
     * Constructs a new Employee to store
     *
     * @param body Creation body to use for the employee, all fields are optional but recommended
     * @return New employee based on the creation fields
     */
    static Employee toEmployee(CreateEmployeeBody body) {
        return new Employee(
                body.firstName,
                body.middleInitial,
                body.lastName,
                Employee.getBirthdayFormatter().parseDateTime(body.birthday),
                Employee.getEmployedAtFormatter().parseDateTime(body.employedAt)
        );
    }
}
