package io.iveelsm.kenzan.create;

import io.iveelsm.kenzan.Employee;
import io.iveelsm.kenzan.cache.Cache;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CreateController {
    private final Cache<UUID, Employee> cache;

    /**
     * Creates an employee.
     *
     * @param body Employee data to enter into the datastore
     * @return This API has a variety of return types:
     *  201 -> Returns upon successful creation, returns an identifier for the created employee
     *  303 -> Returns when an employee already exists, returns a reference to the existing employee
     *  500 -> Returns on an unexpected error in the server.
     */
    @RequestMapping(path = "/employee", method = RequestMethod.POST)
    public ResponseEntity<?> createCustomer(@RequestBody CreateEmployeeBody body) {
        try {
            log.info("Creating employee...");
            var employee = CreateEmployeeBody.toEmployee(body);
            var current = cache.retrieve(employee);
            if(current == null) {
                cache.put(employee.getId(), employee);
                log.info("Employee {} successfully created", employee.getId());
                return new ResponseEntity<>(CreateEmployeeResult.fromEmployee(employee), HttpStatus.CREATED);
            } else {
                log.info("Employee already exists");
                return new ResponseEntity<>(CreateEmployeeResult.fromEmployee(current), HttpStatus.SEE_OTHER);
            }
        } catch (Exception e) {
            log.error("There was an error on employee creation", e);
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}