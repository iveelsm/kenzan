package io.iveelsm.kenzan.create;

import io.iveelsm.kenzan.Employee;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.UUID;

/**
 * This is the JSON object representation of the return type of the PUT /employee/{id} route in the API.
 *
 * This refers to only the UUID as an interpretation of RFC 7231 (https://tools.ietf.org/html/rfc7231)
 *
 * Modifications may require backwards compatibility.
 */
@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
class CreateEmployeeResult {
    private final UUID id;

    /**
     * Constructs the JSON return POJO from a new or existing Employee.
     *
     * @param e Employee to use for construction of the POJO
     * @return A JSON object representing the response to POST /employee route
     */
    static CreateEmployeeResult fromEmployee(Employee e) {
        return new CreateEmployeeResult(e.getId());
    }
}
