package io.iveelsm.kenzan;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.iveelsm.kenzan.cache.mongo.codec.DateTimeDeserializer;
import io.iveelsm.kenzan.cache.mongo.codec.DateTimeSerializer;
import io.iveelsm.kenzan.cache.mongo.codec.DateTimeToDateDeserializer;
import io.iveelsm.kenzan.cache.mongo.codec.DateTimeToDateSerializer;
import lombok.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.UUID;

/**
 * The Employee is the canonical source of information in this API.
 * An Employee is represented by the characteristics defined in this class.
 */
@Data
@NoArgsConstructor
public class Employee {
    // Formats birthdays based on what is expected inputs for the DateTime value
    private static final DateTimeFormatter birthdayFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    // Formats employed at dates based on what is expected inputs for the DateTime value
    private static final DateTimeFormatter employedAtFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");

    private UUID id;
    private String firstName;
    private Character middleInitial;
    private String lastName;
    @JsonSerialize(using = DateTimeToDateSerializer.class)
    @JsonDeserialize(using = DateTimeToDateDeserializer.class)
    private DateTime birthday;
    @JsonSerialize(using = DateTimeToDateSerializer.class)
    @JsonDeserialize(using = DateTimeToDateDeserializer.class)
    private DateTime employedAt;
    private Status status;
    @JsonSerialize(using = DateTimeSerializer.class)
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private DateTime createdAt;
    @JsonSerialize(using = DateTimeSerializer.class)
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private DateTime updatedAt;


    public Employee(UUID id, String firstName, Character middleInitial, String lastName, DateTime birthday, DateTime employedAt, Status status, DateTime createdAt, DateTime updatedAt) {
        this.id = id;
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.birthday = birthday;
        this.employedAt = employedAt;
        this.status = status;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Employee(UUID id, String firstName, Character middleInitial, String lastName, DateTime birthday, DateTime employedAt) {
        this.id = id;
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.birthday = birthday;
        this.employedAt = employedAt;
        this.status = Status.ACTIVE;
    }

    public Employee(String firstName, Character middleInitial, String lastName, DateTime birthday, DateTime employedAt) {
        this.id = UUID.randomUUID();
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.birthday = birthday;
        this.employedAt = employedAt;
        this.status = Status.ACTIVE;
        this.createdAt = DateTime.now(DateTimeZone.UTC);
        this.updatedAt = DateTime.now(DateTimeZone.UTC);
    }

    /**
     * An Employee Status is the indication of whether or not they still work here.
     * Inactive employees are no longer employed
     */
    public enum Status {
        ACTIVE,
        INACTIVE
    }

    public static DateTimeFormatter getBirthdayFormatter() {
        return birthdayFormatter;
    }

    public static DateTimeFormatter getEmployedAtFormatter() {
        return employedAtFormatter;
    }
}
