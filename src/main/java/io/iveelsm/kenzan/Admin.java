package io.iveelsm.kenzan;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.iveelsm.kenzan.cache.mongo.codec.DateTimeDeserializer;
import io.iveelsm.kenzan.cache.mongo.codec.DateTimeSerializer;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * An Admin is a means of managing employee status in the application.
 * Admins are the only ones capable of set employees to Active or Inactive {@link io.iveelsm.kenzan.Employee.Status statuses}
 */
@Data
@NoArgsConstructor
public class Admin {
    private String username;
    private String password;
    @JsonSerialize(using = DateTimeSerializer.class)
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private DateTime createdAt;
    @JsonSerialize(using = DateTimeSerializer.class)
    @JsonDeserialize(using = DateTimeDeserializer.class)
    private DateTime updatedAt;
    private AdminStatus status;

    public enum AdminStatus {
        ACTIVE,
        INACTIVE
    }

    public Admin(String username, String password) {
        this.username = username;
        this.password = password;
        this.createdAt = DateTime.now(DateTimeZone.UTC);
        this.updatedAt = DateTime.now(DateTimeZone.UTC);
        this.status = AdminStatus.ACTIVE;
    }
}
